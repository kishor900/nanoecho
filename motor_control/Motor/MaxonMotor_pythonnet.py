from inspect import getfile, currentframe
from os.path import dirname, abspath
import sys
currentDir = dirname(abspath(getfile(currentframe())))
parentDir = dirname(currentDir)
sys.path.insert(0, parentDir) 
import numpy as np
import time
import clr
import ctypes
from Magnet.MagnetControl import MagnetControl
clr.AddReference('EposLibsNet/EposCmd.Net')
EposCmd64 = ctypes.CDLL('EposLibsNet/EposCmd64.dll')
from EposCmd.Net.VcsWrapper import Device


class MaxonMotor(MagnetControl):

    def __init__(self):
        self.device = Device
        self.nodeID = 1
        self.baudRate = 1000000
        self.keyHandle = 0
        self.timeOut = 500
        self.error = 0
        self.errorCode = 0
        self.speedSetPoints = []
        self.speedDurations = []
        self.positionSetPoints = []
        self.profileSpeed = 0
        self.profileAcc = 0
        self.profileDec = 0
        self.operatingMode = 0
        super().__init__()

    def __initialize(self):
        self.device.Init()
        self.keyHandle, _ = self.device.VcsOpenDevice('EPOS4', 'MAXON SERIAL V2', 'USB', 'USB0', self.errorCode)
        self.device.VcsSetProtocolStackSettings(self.keyHandle, self.baudRate, self.timeOut, self.errorCode)
        self.device.VcsClearFault(self.keyHandle, self.nodeID, self.errorCode)
        self.device.VcsSetMotorType(self.keyHandle, self.nodeID, 1, self.errorCode)
        self.device.VcsSetEnableState(self.keyHandle, self.nodeID, self.errorCode)

    def Setup(self):
        self.__initialize()

    def setOperatingMode(self, mode):
        self.operatingMode = mode

    def setMagnetSpeedParameters(self, speedSetPoints, speedDurations):
        self.speedSetPoints = [np.divide(x*60*75, 19) for x in speedSetPoints]
        self.speedDurations = speedDurations

    def setMagnetPositionParameters(self, positionSetPoints, profileSpeed, repeat, profileAcceleration, profileDeceleration):
        self.positionSetPoints = [x*22.4561404 for x in positionSetPoints] #degree*512*2*2*75/(19*360)
        self.profileSpeed = np.divide(profileSpeed*60*75, 19)
        self.profileAcc = profileAcceleration
        self.profileDec = profileDeceleration

    def startMotor(self):
        if self.operatingMode == 0:
            self.device.VcsSetOperationMode(self.keyHandle, self.nodeID, -2, self.errorCode)
            for i in range(len(self.speedSetPoints)):
                self.device.VcsMoveWithVelocity(self.keyHandle, self.nodeID, self.speedSetPoints[i], self.errorCode)
                time.sleep(self.speedDurations[i])
        else:
            self.device.VcsSetOperationMode(self.keyHandle, self.nodeID, 1, self.errorCode)
            self.device.VcsSetPositionProfile(self.keyHandle, self.nodeID, self.profileSpeed, self.profileAcc, self.profileDec, self.errorCode)
            for i in range(len(self.positionSetPoints)):
                self.device.VcsMoveToPosition(self.keyHandle, self.nodeID, self.positionSetPoints[i], True, False, self.errorCode)
                self.device.VcsWaitForTargetReached(self.keyHandle, self.nodeID, 100000, self.errorCode)

    def closeDevice(self):
        self.device.VcsSetDisableState(self.keyHandle, self.nodeID, self.errorCode)
        self.device.VcsCloseDevice(self.keyHandle, self.errorCode)


def main():

    
    operatingMode = 0 #0 = Speed mode, 1 = Position mode

    #Velocity mode parameters
    speedSetPoints = [1, 5, 2, 10, 3]
    speedDurations = [1, 1, 1, 1, 1]

    #Position mode parameters
    positionSetPoints = [0, 180, 0, 180, 0, 180, 0, 180, 0, 180]
    profileSpeed = 10
    repeat = True
    profileAcceleration = 100000
    profileDeceleration = 100000

    motor = MaxonMotor() 

    motor.setMagnetSpeedParameters(speedSetPoints, speedDurations)
    motor.setMagnetPositionParameters(positionSetPoints, profileSpeed, repeat, profileAcceleration, profileDeceleration)

    motor.setOperatingMode(operatingMode)

    motor.startMotor()

    motor.closeDevice()


if __name__ == '__main__':
    main()
