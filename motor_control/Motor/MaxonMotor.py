import sys
from inspect import getfile, currentframe
from os.path import dirname, abspath

current_dir = dirname(abspath(getfile(currentframe())))
parent_dir = dirname(current_dir)
sys.path.insert(0, parent_dir)
from Magnet.MagnetControl import MagnetControl

from os import chdir, add_dll_directory
from time import sleep
from ctypes import *
import numpy as np

chdir(current_dir)
add_dll_directory(current_dir)
cdll.LoadLibrary('EposCmd64.dll')


class MaxonMotor(MagnetControl):

    def __init__(self):
        self.driver = WinDLL('EposCmd64.dll')
        self.node_ID = 1
        self.baud_rate = 1000000
        self.keyhandle = 0
        self.time_out = 500
        self.error_code = c_uint()
        self.magnet_speeds = []
        self.magnet_speed_durations = []
        self.positions = []
        self.magnet_speed_profile = 0
        self.jerk = 0
        self.operating_mode = -2
        super().__init__()

    def __initialize(self):
        self.keyhandle = self.driver.VCS_OpenDevice('EPOS4'.encode('utf-8'), 'MAXON SERIAL V2'.encode('utf-8'),
                                                    'USB'.encode('utf-8'), 'USB0'.encode('utf-8'),
                                                    byref(self.error_code))
        self.driver.VCS_SetMotorType(self.keyhandle, self.node_ID, 1, byref(self.error_code))
        self.driver.VCS_SetProtocolStackSettings(self.keyhandle, self.baud_rate, self.time_out, byref(self.error_code))
        self.driver.VCS_ClearFault(self.keyhandle, self.node_ID, byref(self.error_code))
        self.driver.VCS_SetObject(self.keyhandle, self.node_ID, c_ushort(0x6080), c_ubyte(0x00), byref(c_void_p(2369)),
                                  c_uint(4), byref(c_uint()), byref(self.error_code))  # Max motor speed = 2369
        self.driver.VCS_SetObject(self.keyhandle, self.node_ID, c_ushort(0x3000), c_ubyte(0x02),
                                  byref(c_void_p(0x11111)), c_uint(4), byref(c_uint()),
                                  byref(self.error_code))  # Set system to "System with gear"
        self.driver.VCS_SetObject(self.keyhandle, self.node_ID, c_ushort(0x3003), c_ubyte(0x01), byref(c_void_p(75)),
                                  c_uint(4), byref(c_uint()), byref(self.error_code))  # Gear ration numerator = 75
        self.driver.VCS_SetObject(self.keyhandle, self.node_ID, c_ushort(0x3003), c_ubyte(0x02), byref(c_void_p(19)),
                                  c_uint(4), byref(c_uint()), byref(self.error_code))  # Gear ration denominator = 19
        self.driver.VCS_SetObject(self.keyhandle, self.node_ID, c_ushort(0x3003), c_ubyte(0x03), byref(c_void_p(2369)),
                                  c_uint(4), byref(c_uint()), byref(self.error_code))  # Gear max speed = 2369
        self.driver.VCS_SetObject(self.keyhandle, self.node_ID, c_ushort(0x3010), c_ubyte(0x01), byref(c_void_p(512)),
                                  c_uint(4), byref(c_uint()),
                                  byref(self.error_code))  # Set number of pulses per revolution
        self.driver.VCS_SetObject(self.keyhandle, self.node_ID, c_ushort(0x607F), c_ubyte(0x00), byref(c_void_p(2369)),
                                  c_uint(4), byref(c_uint()), byref(self.error_code))  # Max profile speed = 2369
        self.driver.VCS_SetEnableState(self.keyhandle, self.node_ID, byref(self.error_code))

    def Setup(self):
        self.__initialize()

    def SetOperatingMode(self, operating_mode):
        self.operating_mode = operating_mode
        self.driver.VCS_SetOperationMode(self.keyhandle, self.node_ID, c_int(self.operating_mode))

    def MagnetSpeedParameters(self, hz, delta_time, interpolation_type):
        self.magnet_speeds = [c_long(int(np.divide(x * 60 * 75, 19))) for x in hz]
        self.magnet_speed_durations = delta_time

    def MagnetPositionParameters(self, positions, magnet_speed, jerk):
        self.positions = [c_long(int(x * 22.4561404)) for x in positions]  # degree*512*2*2*75/(19*360)
        self.magnet_speed_profile = c_long(int(np.divide(magnet_speed * 60 * 75, 19)))
        self.jerk = c_long(int(np.divide(jerk * 60 * 75, 19)))
        self.driver.VCS_SetPositionProfile(self.keyhandle, self.node_ID, self.magnet_speed_profile, self.jerk,
                                           self.jerk, byref(self.error_code))

    def RunIndefinately(self, hz):
        self.SetOperatingMode(-2)
        self.driver.VCS_MoveWithVelocity(self.keyhandle, self.node_ID, c_long(int(np.divide(hz * 60 * 75, 19))))
        print('Motor running at {}Hz.'.format(hz), end='\r')
        while True:
            pass

    def StartMagnet(self):
        if self.operating_mode == -2:
            for i in range(len(self.magnet_speeds)):
                self.driver.VCS_MoveWithVelocity(self.keyhandle, self.node_ID, self.magnet_speeds[i])
                sleep(self.magnet_speed_durations[i])
        else:
            for i in range(len(self.positions)):
                self.driver.VCS_MoveToPosition(self.keyhandle, self.node_ID, self.positions[i], True, False,
                                               byref(self.error_code))
                self.driver.VCS_WaitForTargetReached(self.keyhandle, self.node_ID, 100000, byref(self.error_code))

    def StopMagnet(self):
        self.driver.VCS_SetDisableState(self.keyhandle, self.node_ID, byref(self.error_code))
        self.driver.VCS_CloseDevice(self.keyhandle, byref(self.error_code))
