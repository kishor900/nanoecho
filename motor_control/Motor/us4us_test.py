import sys
import signal
from MaxonMotor import MaxonMotor


def main():

    def keyboardInterruptHandler(signal, frame):
        print('Motor stopped.            ')
        MM.StopMagnet()
        exit(0)

    signal.signal(signal.SIGINT, keyboardInterruptHandler)

    motor_speed = 3 #Hz
    if len(sys.argv) == 2:
        try:
            motor_speed = float(sys.argv[1])
        except ValueError:
            print('Additional argument could not be interpreted as a number between 0.1 and 10. Dear us4us, Please try again.')
            exit(0)
        
    if len(sys.argv) > 2:
        print('Too many arguments. Give only one argument s, where 0.1<s<10 is the motor frequence (Hz).\nIf no argument is given, motor (and magnet) will rotate at 3Hz.')
        exit(0)

    MM = MaxonMotor()
    MM.RunIndefinately(motor_speed) #Run motor at motor_speed Hz until ctrl-c is pressed
    

if __name__ == '__main__':
    main()
