from MaxonMotor import MaxonMotor


def main():

    MM = MaxonMotor()

    MM.MagnetSpeedParameters([5], [10], 0)
    MM.MagnetPositionParameters([0, 180, 0, 180, 0, 180, 0], 1, 10000)
    MM.SetOperatingMode(-2)  #-2 = Speed mode, 1 = Position mode
    MM.StartMagnet()
    MM.StopMagnet()


if __name__ == '__main__':
    main()

