class MagnetControl(object):
    """Magnet Control in MMUS mode"""

    def __init__(self):
        self.Setup()

    """Sets up control parameters for the magnetic field.
    """
    def Setup(self):
        raise NotImplementedError
   
    """Sets up control parameters for the magnetic field.
    """
    def MagnetSpeedParameters(self, hz, delta_time, interpolation_type):
        raise NotImplementedError

    def MagnetPositionParameters(self, positions, magnet_speed, jerk):
        raise NotImplementedError

    def MagnetFieldStrength(self, field_tesla):
        raise NotImplementedError

    def SetMagnetSpeed(self, frequency_hz):
        raise NotImplementedError

    def SetOperatingMode(self, mode):
        raise NotImplementedError

    def BmodeCallback(self):
        raise NotImplementedError

    def MmusModeCallback(self):
        raise NotImplementedError

    def StartMagnet(self):
        raise NotImplementedError

    def StopMagnet(self):
        raise NotImplementedError

    def MagnetStatus(self):
        raise NotImplementedError
