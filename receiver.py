import zmq
import json
from threading import Thread
import api
import queue

zmq_sys = zmq.Context().socket(zmq.PULL)
zmq_sys.bind('tcp://127.0.0.1:' + str(1010))

zmq_flag = zmq.Context().socket(zmq.PULL)
zmq_flag.bind('tcp://127.0.0.1:' + str(1020))

zmq_data = zmq.Context().socket(zmq.PULL)
zmq_data.bind('tcp://127.0.0.1:' + str(1030))

sender_1 = zmq.Context().socket(zmq.PUSH)
sender_1.connect('tcp://127.0.0.1:' + str(1040))

sender_2 = zmq.Context().socket(zmq.PUSH)
sender_2.connect('tcp://127.0.0.1:' + str(1050))


def stream_feed(q):
    while True:
        try:
            print("waiting for stop flag")
            stop_flag = q.get_nowait()
        except:
            stop_flag = "pass"
        if stop_flag == "stop":
            break
        bts = api.stream_bmode_images_fake()
        sender_2.send_string(bts)


q = queue.Queue(maxsize=1)

image_streaming = False
motor_started = False


def system_checks():  # this function is to check sw/hw state
    global image_streaming
    while True:
        data = zmq_sys.recv_string()
        print(data)
        data_dict = json.loads(data)
        c_type = data_dict["C_Type"]

        if c_type == "UtoP":
            utop_input = data_dict["UtoP_Input"]
            JSONReply = {}
            JSONReply["C_Type"] = "PtoU"
            if utop_input == "PowerUp":
                powerup_input = data_dict["PowerUp_Input"]
                JSONReply["PtoU_Input"] = "PowerUp"
                JSONReply["PowerUp_Input"] = powerup_input
                if powerup_input == "sw_check":
                    sw_check = api.sw_check()
                    if sw_check:
                        JSONReply["PowerUp_Response"] = "Success"
                    else:
                        JSONReply["PowerUp_Response"] = "Failed"
                elif powerup_input == "pohd_check":
                    # hw_check = api.hw_check()
                    hw_check = True
                    if hw_check:
                        JSONReply["PowerUp_Response"] = "Success"
                    else:
                        JSONReply["PowerUp_Response"] = "Failed"
                elif powerup_input == "pos_check":
                    scanner_check = api.mm_check()
                    if scanner_check:
                        JSONReply["PowerUp_Response"] = "Success"
                    else:
                        JSONReply["PowerUp_Response"] = "Failed"
            elif utop_input == "Main":
                Main_Input = data_dict["Main_Input"]
                JSONReply["PtoU_Input"] = "Main"
                JSONReply["Main_Input"] = Main_Input
                if Main_Input == "Scan":
                    if not image_streaming:
                        image_streaming = True
                        Thread(target=stream_feed, args=[q]).start()
                        JSONReply["Scan_Response"] = "Success"
                    else:
                        JSONReply["Scan_Response"] = "Failed"
                if Main_Input == "Freeze":
                    image_streaming = False
                    q.put("stop")
                    JSONReply["Freeze_Response"] = "Success"
                elif Main_Input == "MMUS":
                    if not motor_started:
                        motor_started = True
                        x = api.set_mm_speed(5)
                        if x:
                            JSONReply["MMUS_Response"] = "Success"
                        else:
                            JSONReply["MMUS_Response"] = "Failed"
                    else:
                        JSONReply["MMUS_Response"] = "Failed"

        JSONReply_string = json.dumps(JSONReply)
        sender_1.send_string(JSONReply_string)


Thread(target=system_checks).start()
