# import hw_api as us4us
from motor_control.Motor import MaxonMotor as mm
# import cv2
# from PIL import Image
import io
import base64
import numpy
import numpy as np
import time


def hw_check():
    try:
        image = us4us.get_bmode()
        if image.shape:
            return True
        else:
            return False
    except:
        return False


def sw_check():
    return True


mm_obj = mm.MaxonMotor()


def mm_check():
    try:
        mm_obj.Setup()
        return True
    except:
        return False


def set_mm_speed(speed):
    mm_obj.Setup()
    x = mm_obj.RunIndefinately(speed)
    return x


def stream_bmode_images():
    mat = us4us.get_bmode()
    PIL_image = Image.fromarray(np.uint8(mat))
    bts = str(PIL_image.tobytes())
    return bts


def stream_bmode_images_fake():
    time.sleep(0.9)
    with open(r"A:\Work\Company\Neosoft\NanoEcho_python\a.txt", "r") as f:
        bts = f.read()
    return bts


